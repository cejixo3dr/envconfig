//go:build appengine || go1.5
// +build appengine go1.5

package envconfig

import "os"

var lookupEnv = os.LookupEnv

type multiLookup struct {
	def       func(key string) (string, bool)
	lookupers []func(key string) (string, bool)
}

func newMultiLookup(lookupers ...func(key string) (string, bool)) *multiLookup {
	l := &multiLookup{
		def:       lookupEnv,
		lookupers: lookupers,
	}
	return l
}

func (l *multiLookup) lookup(key string) (string, bool) {
	for _, lookup := range l.lookupers {
		v, ok := lookup(key)
		if ok {
			return v, true
		}

	}

	return l.def(key)
}
